package com.example.databaseapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

public class User_List_Adapter extends BaseAdapter {
    Context context;
    ArrayList<HashMap<String,Object>> userList;
    public User_List_Adapter(Context context, ArrayList<HashMap<String,Object>> userList){
        this.context=context;
        this.userList=userList;

    }
    @Override
    public int getCount( )
    {
        return userList.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        View view1 = LayoutInflater.from(context).inflate(R.layout.view_user_list,null);
        TextView  Name=view1.findViewById(R.id.NAME);
        TextView  Email=view1.findViewById(R.id.EMAIL);
        TextView  Password=view1.findViewById(R.id.PASSWORD);
        TextView  checkbox=view1.findViewById(R.id.CHECKBOX);
        TextView gender=view1.findViewById(R.id.GENDER);
        Name.setText(String.valueOf(userList.get(position).get("name")));
        Email.setText(String.valueOf(userList.get(position).get("email")));
        Password.setText(String.valueOf(userList.get(position).get("password")));
        gender.setText(String.valueOf(userList.get(position).get("Gender")));
        checkbox.setText(String.valueOf(userList.get(position).get("checkBox")));
        return view1;
    }
}
