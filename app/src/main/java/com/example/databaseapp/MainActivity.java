package com.example.databaseapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    EditText e1,e2,e3;
    Button b;
    RadioGroup radioGroup;
    ImageView imageView;
    RadioButton getRadioButton;
    CheckBox Kabaddi,Cricket,Football;
    ArrayList<String> mResult;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        imageView=findViewById(R.id.img);
        e1=findViewById(R.id.name);
        e2=findViewById(R.id.email);
        e3=findViewById(R.id.password);
        b=findViewById(R.id.submit);
        Kabaddi=findViewById(R.id.kabaddi);
        Cricket=findViewById(R.id.cricket);
        Football=findViewById(R.id.football);
       radioGroup=findViewById(R.id.gender);
   Football.setVisibility(View.INVISIBLE);
      mResult= new ArrayList<>();
        Kabaddi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Kabaddi.isChecked())
                    mResult.add("Kabaddi");
                else
                    mResult.remove("Kabaddi");
            }
        });

        Cricket.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Cricket.isChecked())
                    mResult.add("Cricket");
                else
                    mResult.remove("Cricket");
            }
        });
        Football.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Football.isChecked())
                    mResult.add("FootBall");
                else
                    mResult.remove("Football");
            }
        });
       imageView.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               finish();
           }
       });
       radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
           @Override
           public void onCheckedChanged(RadioGroup radioGroup, int i) {
               if (i== R.id.male)
               {
                   Kabaddi.setVisibility(View.VISIBLE);
                   Cricket.setVisibility(View.VISIBLE);
                   Football.setVisibility(View.VISIBLE);
               }
               else if (i == R.id.female)
               {
                   Kabaddi.setVisibility(View.VISIBLE);
                   Cricket.setVisibility(View.VISIBLE);
                   Football.setVisibility(View.INVISIBLE);
               }
           }
       });
       b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isValid()) {
                    String name = e1.getText().toString();
                    String email = e2.getText().toString();
                    String password = e3.getText().toString();
                    String radiobutton = getRadioButton.getText().toString();
                    Intent i = new Intent(MainActivity.this, MainActivity2.class);
                    StringBuilder stringBuilder = new StringBuilder();
                    for (String s : mResult)
                        stringBuilder.append(s).append("\n");
                    String game = stringBuilder.toString();
                    i.putExtra("Gender", radiobutton);
                    i.putExtra("name", name);
                    i.putExtra("email", email);
                    i.putExtra("password", password);
                    i.putExtra("checkBox", game);

                    startActivity(i);
                }
            }
        });

    }

   public void checkButton (View v)
   {
       int radiobuttonid= radioGroup.getCheckedRadioButtonId();
       getRadioButton=findViewById(radiobuttonid);
   }
    public boolean isValid()
    {
        boolean flag = true;
        if(TextUtils.isEmpty(e1.getText())) {
            {
                e1.setError(getString(R.string.error_enter_Name));
                flag = false;
            }
        }
        if(TextUtils.isEmpty(e2.getText())) {
            {
                e2.setError(getString(R.string.error_enter_email));
                flag = false;
            }
        }
            else{
                String email =e2.getText().toString().trim();
                String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
                if (!email.matches(emailPattern))
                {
                    flag=false;
                    e2.setError(getString(R.string.error_enter_valid_email));
                }

        }
        if(TextUtils.isEmpty(e3.getText())) {
            {
                e3.setError(getString(R.string.error_enter_password));
                flag = false;
            }
        }
        return  flag;
    }
}