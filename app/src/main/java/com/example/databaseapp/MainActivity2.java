package com.example.databaseapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

public class MainActivity2 extends AppCompatActivity {

ListView listView;
ArrayList<HashMap<String,Object>> userList = new ArrayList<>();
User_List_Adapter user_list_adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        getSupportActionBar().setTitle("Secound Activity");
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        bindViewValues();
        initViewReference();
    }
    void bindViewValues()
    {

        userList.addAll((Collection<? extends HashMap<String,Object>>) getIntent().getSerializableExtra("userList"));
        user_list_adapter= new User_List_Adapter(this);
        listView.setAdapter(user_list_adapter);
    }
    void initViewReference()
    {
        listView= findViewById(R.id.USERLIST);
        for (int i=0;i<userList.size();i++){
            View view= LayoutInflater.from(MainActivity2.this).inflate(R.layout.view_user_list,null);
            listView.addView(view);
        }
    }
}